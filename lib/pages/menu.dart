import 'package:ali_auto_managment/pages/clients_page.dart';
import 'package:ali_auto_managment/pages/commandes_page.dart';
import 'package:ali_auto_managment/widgets/menu_button.dart';
import 'package:flutter/material.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        MenuButton(
          'Clients',
          Icons.account_circle,
          _buttonNouveauClientPressed,
        ),
        MenuButton(
          'Commandes',
          Icons.monetization_on,
          _buttonNouvelleCommandePressed,
        ),
      ],
    );
  }

  _buttonNouveauClientPressed() {
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    //await prefs.setString('user', encodedUser);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ClientsPage()),
    );
  }

  _buttonNouvelleCommandePressed() {
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    //await prefs.setString('user', encodedUser);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CommandesPage()),
    );
  }
}
