import 'dart:convert';

import 'package:ali_auto_managment/models/client.dart';
import 'package:ali_auto_managment/pages/single_client_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ClientsPage extends StatefulWidget {
  @override
  _ClientsPageState createState() => _ClientsPageState();
}

class _ClientsPageState extends State<ClientsPage> {
  final _listAnimatedKey = GlobalKey<AnimatedListState>();
  final _clientNameController = TextEditingController();
  final _searchQuery = TextEditingController();
  final _clientTelephoneController = TextEditingController();
  bool _error = false;
  List _clients = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Clients'),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              if (_searchQuery.text == '') {
                showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      _createClientPopup(context),
                );
              } else {
                showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      _errorCreateClientPopup(context),
                );
              }
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          TextField(
            controller: _searchQuery,
            onChanged: (text) async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              _clients =
                  jsonDecode(prefs.getString('clients') ?? jsonEncode([]));
              if (text.isNotEmpty) {
                setState(() {
                  _clients = _clients.map((c) {
                    Client _client = Client.fromJSON(c);
                    if (_client.name.toLowerCase().startsWith(text)) {
                      return c;
                    }
                  }).toList();
                });
              } else {
                _getClients();
              }
            },
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.search),
              labelText: 'Rechercher une personne',
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Spacer(),
                Expanded(
                  flex: 5,
                  child: _conditionnalDisplay(),
                ),
                Spacer(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _conditionnalDisplay() {
    if (_clients.length > 0) {
      return AnimatedList(
        key: _listAnimatedKey,
        initialItemCount: _clients.length,
        itemBuilder: (context, index, animation) {
          if (_clients[index] != null) {
            Client _clientDisplay = Client.fromJSON(_clients[index]);
            return SlideTransition(
              child: _buildClient(_clientDisplay),
              position: animation.drive(
                Tween<Offset>(
                  begin: Offset(-2.0, 0.0),
                  end: Offset.zero,
                ).chain(
                  CurveTween(curve: Curves.ease),
                ),
              ),
            );
          } else {
            return SlideTransition(
              child: Container(),
              position: animation.drive(
                Tween<Offset>(
                  begin: Offset(-2.0, 0.0),
                  end: Offset.zero,
                ).chain(
                  CurveTween(curve: Curves.ease),
                ),
              ),
            );
          }
        },
      );
    } else {
      _getClients();
      return Text('Récupération des clients');
    }
  }

  _getClients() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _clients = jsonDecode(prefs.getString('clients') ?? jsonEncode([]));
    });
  }

  Widget _errorCreateClientPopup(BuildContext context) {
    return AlertDialog(
      title: Text('Impossible d\'ajouter un client'),
      content: Text(
        'Vous ne pouvez pas ajouter de client car vous êtes en mode recherche. Enlever la recherche pour pouvoir ajouter un client',
        style: TextStyle(
          color: Colors.red,
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'Fermer',
          ),
        ),
      ],
    );
  }

  Widget _createClientPopup(BuildContext context) {
    return AlertDialog(
      title: Text('Entrez le nom du client'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            controller: _clientNameController,
            decoration: InputDecoration(
              labelText: 'Nom',
              errorText: _error ? 'Le texte est vide' : null,
            ),
          ),
          TextField(
            controller: _clientTelephoneController,
            decoration: InputDecoration(
              labelText: 'Téléphone',
            ),
          ),
        ],
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'Annuler',
            style: TextStyle(
              color: Colors.red,
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            if (_clientNameController.text.isNotEmpty) {
              Client _client = Client(_clientNameController.text,
                  _clientTelephoneController.text, []);
              setState(() {
                _error = false;
                _clients.insert(0, _client.toJSON());
                if (_clients.length > 1) {
                  _listAnimatedKey.currentState
                      .insertItem(0, duration: Duration(milliseconds: 700));
                }
                Navigator.of(context).pop();
                _storageClient(_clients);
                _clientNameController.text = '';
                _clientTelephoneController.text = '';
              });
            } else {
              Navigator.of(context).pop();
              setState(() {
                _error = true;
              });
              showDialog(
                context: context,
                builder: (BuildContext context) => _createClientPopup(context),
              );
            }
          },
          child: Text('Créer'),
        ),
      ],
    );
  }

  _storageClient(_newClientsList) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('clients', jsonEncode(_newClientsList));
  }

  Widget _buildClient(_clientDisplayItem) {
    return GestureDetector(
      onTap: () {
        _goToClient(_clientDisplayItem);
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.deepPurple,
            borderRadius: BorderRadius.all(
              Radius.circular(4.0),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.account_circle,
                  color: Colors.white,
                ),
                Container(
                  width: 8.0,
                ),
                Flexible(
                  child: Text(
                    _clientDisplayItem.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _goToClient(_clientDatas) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SingleClientPage(_clientDatas),
      ),
    );
  }
}
