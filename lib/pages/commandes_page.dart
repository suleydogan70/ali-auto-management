import 'dart:convert';

import 'package:ali_auto_managment/models/commande.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CommandesPage extends StatefulWidget {
  @override
  _CommandesPage createState() => _CommandesPage();
}

class _CommandesPage extends State<CommandesPage> {
  final _listAnimatedKey = GlobalKey<AnimatedListState>();
  List _commandes = [];
  List _clients = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Commandes'),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) =>
                    _createCommandePopup(context),
              );
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
      body: Container(
        child: _conditionnalDisplay(),
      ),
    );
  }

  _getCommandes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _clients = jsonDecode(prefs.getString('clients') ?? jsonEncode([]));
      _commandes = jsonDecode(prefs.getString('commandes') ?? jsonEncode([]));
    });
  }

  Widget _conditionnalDisplay() {
    if (_commandes.length > 0) {
      return AnimatedList(
        key: _listAnimatedKey,
        initialItemCount: _commandes.length,
        itemBuilder: (context, index, animation) {
          Commande _commandeDisplay = Commande.fromJSON(_commandes[index]);
          return SlideTransition(
            child: _buildCommande(_commandeDisplay),
            position: animation.drive(
              Tween<Offset>(
                begin: Offset(-2.0, 0.0),
                end: Offset.zero,
              ).chain(
                CurveTween(curve: Curves.ease),
              ),
            ),
          );
        },
      );
    } else {
      _getCommandes();
      return Text('Récupération des commandes');
    }
  }

  Widget _buildCommande(commande) {
    return Text(commande.voiture);
  }

  Widget _createCommandePopup(BuildContext context) {
    return AlertDialog(
      title: Text('Entrez les informations de la commande'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DropdownButton(
            value: null,
            onChanged: (value) {},
            icon: Icon(Icons.arrow_downward),
            elevation: 16,
            style: TextStyle(color: Colors.deepPurple),
            items: ['One', 'Two', 'Free', 'Four'].map((String value) {
              return DropdownMenuItem(
                value: value,
                child: Text(value),
              );
            }).toList(),
          )
        ],
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'Annuler',
            style: TextStyle(
              color: Colors.red,
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            print('create commande');
          },
          child: Text('Créer'),
        ),
      ],
    );
  }
}
