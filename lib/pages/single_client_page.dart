import 'package:ali_auto_managment/models/client.dart';
import 'package:flutter/material.dart';

class SingleClientPage extends StatefulWidget {
  final Client client;

  SingleClientPage(this.client);

  @override
  _SingleClientPageState createState() => _SingleClientPageState();
}

class _SingleClientPageState extends State<SingleClientPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.client.name),
      ),
      body: Row(
        children: <Widget>[
          Spacer(),
          Expanded(
            child: Column(
              children: <Widget>[
                Container(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      'Informations générales',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    Text('Téléphone : ${widget.client.phone}')
                  ],
                ),
                Container(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      'Commandes',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 12.0,
                ),
                Row(
                  children: widget.client.userCommandes.length == 0
                      ? [Text('Aucune commandes pour cette personne')]
                      : widget.client.userCommandes
                          .map((commande) => Container(
                                color:
                                    (commande.paye ? Colors.green : Colors.red),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Icon(Icons.directions_car),
                                        Text(commande.voiture),
                                      ],
                                    ),
                                    Container(
                                      height: 12.0,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text('Description de la prestation'),
                                        Text(commande.description),
                                      ],
                                    ),
                                    Container(
                                      height: 12.0,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Icon(Icons.euro_symbol),
                                        Text(commande.prix),
                                      ],
                                    ),
                                  ],
                                ),
                              ))
                          .toList(),
                ),
              ],
            ),
            flex: 5,
          ),
          Spacer(),
        ],
      ),
    );
  }
}
