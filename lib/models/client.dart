import 'package:ali_auto_managment/models/commande.dart';

class Client {
  String name;
  String phone;
  List userCommandes;

  Client(this.name, this.phone, this.userCommandes);

  Client.fromJSON(Map<String, dynamic> json) {
    name = json['name'] ?? '';
    phone = json['phone'] ?? '';
    userCommandes = json['userCommandes'] ?? [];
  }

  Map<String, dynamic> toJSON() {
    return {
      'name': name,
      'phone': phone,
      'userCommandes': userCommandes,
    };
  }

  addCommande(Commande commande) {
    userCommandes.add(commande);
  }

  @override
  String toString() {
    return 'Client{name: $name, phone: $phone, userCommandes: $userCommandes}';
  }
}
