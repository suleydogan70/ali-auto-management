class Commande {
  String client;
  int prix;
  String voiture;
  String description;
  bool paye;

  Commande(this.client, this.prix, this.voiture, this.description, this.paye);

  Commande.fromJSON(Map<String, dynamic> json) {
    client = json['client'] ?? '';
    prix = json['prix'] ?? 0;
    voiture = json['voiture'] ?? '';
    description = json['description'] ?? '';
    paye = json['paye'] ?? false;
  }

  Map<String, dynamic> toJSON() {
    return {
      'client': client,
      'prix': prix,
      'voiture': voiture,
      'description': description,
      'paye': paye,
    };
  }

  @override
  String toString() {
    return 'Commande{client: $client, prix: $prix, voiture: $voiture, description: $description, paye: $paye}';
  }
}
